package org.example.boxoffice.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@WebAppConfiguration
@Rollback
public class UserJourneyTest {
    public static final long SHOW_ID = 1;
    public static final long PASSED_SHOW_ID = 4;
    public static final String SEAT_URL = "/v1/shows/{0}/seats";
    public static final String RESERVATION_URL = "/v1/reservations/{0}";
    public static final String RESERVATION_LIST_URL = "/v1/reservations";
    public static final String SHOW_LIST_URL = "/v1/shows";

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private ObjectMapper objectMapper;
    private MockMvc mockMvc;


    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void asAMovieFanIWantToExploreShows() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_LIST_URL))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", iterableWithSize(3)))
                .andExpect(jsonPath("$[*].movie",
                        containsInAnyOrder("From Dusk Till Dawn", "Reservoir Dogs", "The Hateful Eight")));
    }

    @Test
    public void asAMovieFanIWantToCheckOutForFreePlaces() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SEAT_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    public void asAMovieFanIWhatToBookAPlaceAndGetTheBookingId() throws Exception {
        // When I book a seat
        Reservation reservation = new Reservation();
        reservation.setEmail("name@email.com");
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(1);
        reservationItem.setPlace(4);
        reservationItem.setShowId(1);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_LIST_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        // then I should get a reservation code
        String key = result.getResponse().getContentAsString();
        // and be able to check the reservation
        mockMvc.perform(MockMvcRequestBuilders.get(RESERVATION_URL, key))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is(reservation.getEmail())))
                .andExpect(jsonPath("$.items", iterableWithSize(1)))
                .andExpect(jsonPath("$.items[0].place", is(reservationItem.getPlace())))
                .andExpect(jsonPath("$.items[0].row", is(reservationItem.getRow())))
                .andExpect(jsonPath("$.items[0].showId", is((int) reservationItem.getShowId())));
        // and see that the place got marked as reserved
        mockMvc.perform(MockMvcRequestBuilders.get(SEAT_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.row=='1' && @.place=='4')].status",
                        containsInAnyOrder(SeatStatus.RESERVED.toString())));
    }

    @Test
    public void asAMovieFanIWantToBookPlacesForMyFriendInASingleCall() throws Exception {
        // When I book two seats
        Reservation reservation = new Reservation();
        reservation.setEmail("name@email.com");
        ReservationItem reservationItem1 = new ReservationItem();
        ReservationItem reservationItem2 = new ReservationItem();
        reservation.setItems(Arrays.asList(reservationItem1, reservationItem2));
        reservationItem1.setRow(3);
        reservationItem1.setPlace(5);
        reservationItem1.setShowId(1);
        reservationItem2.setRow(3);
        reservationItem2.setPlace(4);
        reservationItem2.setShowId(1);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_LIST_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        // and see that the places got marked as reserved
        mockMvc.perform(MockMvcRequestBuilders.get(SEAT_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.row=='3' && (@.place=='4' || @.place=='5'))].status",
                        containsInAnyOrder(SeatStatus.RESERVED.toString(), SeatStatus.RESERVED.toString())));
    }

    @Test
    public void asAMovieFanIWantToSeeThatBookingIsAlreadyClosed() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SEAT_URL, PASSED_SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.status!='UNAVAILABLE')]", emptyIterable()));
    }
}
