package org.example.boxoffice.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@WebAppConfiguration
@Rollback
class BookingControllerTest {

    public static final long EXPIRED_SHOW_ID = 4;
    public static final long UNKNOWN_SHOW_ID = 0;
    public static final long SHOW_ID = 1;
    public static final String RESERVATION_URL = "/v1/reservations";
    public static final String SHOW_URL = "/v1/shows/{0}/seats";
    public static final String EMAIL = "zherebjatjew@gmail.com";
    public static final String EXISTING_RESERVATION_KEY = "b89cb27e-307d-4b13-aa59-57898a509e4e";
    public static final String UNKNOWN_RESERVATION_KEY = "68e5f686-594c-424c-8f01-2d2003980f73";

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private ObjectMapper objectMapper;
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void shouldReturnAllShows() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/shows?activeOnly=false"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", iterableWithSize(4)));
    }

    @Test
    public void shouldReturnActiveShows() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/shows"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", iterableWithSize(3)));
    }

    @Test
    public void shouldReturnSpecificShow() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldValidateShowId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_URL, UNKNOWN_SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnSeats() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", iterableWithSize(20)));
    }

    @Test
    public void shouldMarkBookedSeats() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_URL, SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.row==1 && (@.place==1 || @.place==2))].status",
                        containsInAnyOrder("RESERVED", "RESERVED")));
    }

    @Test
    public void shouldRespectShowIdMarkingSeats() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(SHOW_URL, EXPIRED_SHOW_ID))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[?(@.status=='RESERVED')]", emptyIterable()))
                .andExpect(jsonPath("$[?(@.status=='UNAVAILABLE')]", iterableWithSize(20)));
    }

    @Test
    public void shouldBookSeat() throws Exception {
        Reservation reservation = new Reservation();
        reservation.setEmail(EMAIL);
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(3);
        reservationItem.setPlace(2);
        reservationItem.setShowId(SHOW_ID);
        mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().string(matchesPattern("[-0-9a-fA-F]{36}")));
    }

    @Test
    public void shouldDenyBookingOnePlaceTwice() throws Exception {
        Reservation reservation = new Reservation();
        reservation.setEmail(EMAIL);
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(1);
        reservationItem.setPlace(1);
        reservationItem.setShowId(SHOW_ID);
        mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isConflict());
    }

    @Test
    public void shouldDenyBookingNonExistingSeats() throws Exception {
        Reservation reservation = new Reservation();
        reservation.setEmail(EMAIL);
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(0);
        reservationItem.setPlace(3);
        reservationItem.setShowId(SHOW_ID);
        mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Seat at row 0 and 3 does not exists"));
    }

    @Test
    public void shouldDeclineBookingForUnknownShow() throws Exception {
        Reservation reservation = new Reservation();
        reservation.setEmail(EMAIL);
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(1);
        reservationItem.setPlace(3);
        reservationItem.setShowId(UNKNOWN_SHOW_ID);
        mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Show was not found"));
    }

    @Test
    public void shouldDeclineBookingForExpiredShow() throws Exception {
        Reservation reservation = new Reservation();
        reservation.setEmail(EMAIL);
        ReservationItem reservationItem = new ReservationItem();
        reservation.setItems(Collections.singletonList(reservationItem));
        reservationItem.setRow(1);
        reservationItem.setPlace(3);
        reservationItem.setShowId(EXPIRED_SHOW_ID);
        mockMvc.perform(MockMvcRequestBuilders.post(RESERVATION_URL)
                .content(objectMapper.writeValueAsString(reservation)).contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(content().string("It is too late to book places for this show"));
    }

    @Test
    public void shouldObtainReservationByKey() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/reservations/{0}", EXISTING_RESERVATION_KEY))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("zherebjatjew@yandex.ru")));
    }

    @Test
    public void shouldValidateReservationKey() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/reservations/{0}", UNKNOWN_RESERVATION_KEY))
                .andExpect(status().isNotFound());
    }
}