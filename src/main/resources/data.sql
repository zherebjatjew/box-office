delete from `reservation`;
delete from `show`;
delete from `seat`;


insert into `seat` (`id`, `row`, `place`) values (1, 1, 1);
insert into `seat` (`id`, `row`, `place`) values (2, 1, 2);
insert into `seat` (`id`, `row`, `place`) values (3, 1, 3);
insert into `seat` (`id`, `row`, `place`) values (4, 1, 4);
insert into `seat` (`id`, `row`, `place`) values (5, 1, 5);
insert into `seat` (`id`, `row`, `place`) values (6, 2, 1);
insert into `seat` (`id`, `row`, `place`) values (7, 2, 2);
insert into `seat` (`id`, `row`, `place`) values (8, 2, 3);
insert into `seat` (`id`, `row`, `place`) values (9, 2, 4);
insert into `seat` (`id`, `row`, `place`) values (10, 2, 5);
insert into `seat` (`id`, `row`, `place`) values (11, 3, 1);
insert into `seat` (`id`, `row`, `place`) values (12, 3, 2);
insert into `seat` (`id`, `row`, `place`) values (13, 3, 3);
insert into `seat` (`id`, `row`, `place`) values (14, 3, 4);
insert into `seat` (`id`, `row`, `place`) values (15, 3, 5);
insert into `seat` (`id`, `row`, `place`) values (16, 4, 1);
insert into `seat` (`id`, `row`, `place`) values (17, 4, 2);
insert into `seat` (`id`, `row`, `place`) values (18, 4, 3);
insert into `seat` (`id`, `row`, `place`) values (19, 4, 4);
insert into `seat` (`id`, `row`, `place`) values (20, 4, 5);

insert into `show` (`id`, `movie`, `begins`, `ends`) values (1, 'From Dusk Till Dawn', '2020-10-15 14:00', '2020-10-15 15:58');
insert into `show` (`id`, `movie`, `begins`, `ends`) values (2, 'Reservoir Dogs', '2020-10-15 17:00', '2020-10-15 19:25');
insert into `show` (`id`, `movie`, `begins`, `ends`) values (3, 'The Hateful Eight', '2020-10-15 23:20', '2020-10-16 01:45');
insert into `show` (`id`, `movie`, `begins`, `ends`) values (4, 'Kill Bill', '2020-02-15 23:20', '2020-02-16 01:45');

insert into `reservation` (`id`, `seat_id`, `email`, `show_id`, `paid`, `natural_key`) values (1, 1, 'zherebjatjew@gmail.com', 1, false, '22250077-044e-4003-ac54-0f8613b3d460');
insert into `reservation` (`id`, `seat_id`, `email`, `show_id`, `paid`, `natural_key`) values (2, 2, 'zherebjatjew@yandex.ru', 1, false, 'b89cb27e-307d-4b13-aa59-57898a509e4e');


