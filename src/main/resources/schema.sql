create table if not exists `seat`
(
    `id`     long primary key auto_increment,
    `row`    integer not null,
    `place`  integer not null,
    unique key (`row`, `place`)
);


create table if not exists `show`
(
    `id`      long primary key auto_increment,
    `movie`   varchar(200),
    `begins`  datetime,
    `ends`    datetime
);

create table if not exists `reservation`
(
    `id`      long primary key auto_increment,
    `seat_id` long not null,
    `show_id` long not null,
    `email`   varchar(200),
    `paid`    boolean,
    `natural_key` varchar(36),
    unique key (seat_id, show_id),
    foreign key (show_id) references `show`(id),
    foreign key (seat_id) references seat(id)
);

create index reservation_nk on reservation(natural_key);
