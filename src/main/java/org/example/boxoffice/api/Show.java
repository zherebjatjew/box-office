package org.example.boxoffice.api;

import java.time.LocalDateTime;

/**
 * Represents a show of a movie scheduled.
 */
public class Show {
    private long id;
    private String movie;
    private LocalDateTime starts;
    private LocalDateTime ends;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public LocalDateTime getStarts() {
        return starts;
    }

    public void setStarts(LocalDateTime starts) {
        this.starts = starts;
    }

    public LocalDateTime getEnds() {
        return ends;
    }

    public void setEnds(LocalDateTime ends) {
        this.ends = ends;
    }
}
