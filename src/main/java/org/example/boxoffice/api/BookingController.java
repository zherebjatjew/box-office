package org.example.boxoffice.api;

import org.example.boxoffice.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * The controller encapsulates end-user methods like exploring upcoming shows and booking places.
 */
@RestController
@RequestMapping("/v1/")
public class BookingController {
    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    /**
     * Get movie shows.
     *
     * @param activeOnly if true the method returns only the items available for booking
     * @return list of shows
     */
    @GetMapping("shows")
    public Collection<Show> getShows(
            @RequestParam(value = "activeOnly", required = false, defaultValue = "true") boolean activeOnly) {
        return bookingService.getShows(activeOnly);
    }

    /**
     * Get specific show by id.
     *
     * @param showId show id
     * @return show
     */
    @GetMapping("shows/{showId}")
    public Show getShow(@PathVariable("showId") long showId) {
        return bookingService.getShow(showId);
    }

    /**
     * Get seat layout. In general, auditorium can be not rectangular. For example, last row can have less seats.
     * For this reason each seat is represented as a separate item with coordinates accompanied with seat status.
     * The status can be {@code FREE} (available for reservation), {@code RESERVED} (already reserved by somebody)
     * or {@code UNAVAILABLE} by some reasons.
     *
     * @param showId for which show to search for reservations
     * @return list of seats with statuses
     */
    @GetMapping("shows/{showId}/seats")
    public Collection<Seat> getSeats(@PathVariable long showId) {
        return bookingService.getSeats(showId);
    }

    /**
     * Get reservation details by reservation key.
     *
     * @param reservationKey key returned by {@link BookingController#book(Reservation)}
     * @return reservation details
     */
    @GetMapping("reservations/{id}")
    public Reservation getReservation(@PathVariable("id") String reservationKey) {
        return bookingService.getReservation(reservationKey);
    }

    /**
     * Add a reservation. One call can reserve one or more places for one or different shows.
     * @param reservation
     * @return
     */
    @PostMapping("reservations")
    public String book(@RequestBody Reservation reservation) {
        return bookingService.book(reservation);
    }
}
