package org.example.boxoffice.api;

public enum SeatStatus {
    FREE,
    RESERVED,
    UNAVAILABLE
}
