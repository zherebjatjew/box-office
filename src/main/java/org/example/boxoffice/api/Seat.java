package org.example.boxoffice.api;

/**
 * Represents single seat in auditorium.
 */
public class Seat {
    private int place;
    private int row;
    private SeatStatus status;

    public SeatStatus getStatus() {
        return status;
    }

    public void setStatus(SeatStatus status) {
        this.status = status;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("{row=").append(row);
        b.append(", place=").append(place);
        b.append(", status=").append(status == null ? "null" : status.toString()).append("}");
        return b.toString();
    }
}
