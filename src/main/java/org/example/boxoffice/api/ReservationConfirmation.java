package org.example.boxoffice.api;

import java.time.LocalDateTime;

public class ReservationConfirmation {
    private long reservationId;
    private String email;
    private LocalDateTime expirationTime;
}
