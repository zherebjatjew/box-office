package org.example.boxoffice.api;

/**
 * Information about single place in {@link Reservation}.
 */
public class ReservationItem {
    private int place;
    private int row;
    private long showId;

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public long getShowId() {
        return showId;
    }

    public void setShowId(long showId) {
        this.showId = showId;
    }
}
