package org.example.boxoffice.api;

import java.util.List;

/**
 * Seat reservation data.
 */
public class Reservation {
    private String email;
    private List<ReservationItem> items;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ReservationItem> getItems() {
        return items;
    }

    public void setItems(List<ReservationItem> items) {
        this.items = items;
    }
}
