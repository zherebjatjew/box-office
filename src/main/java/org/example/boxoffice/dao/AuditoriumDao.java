package org.example.boxoffice.dao;

import org.example.boxoffice.api.SeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.*;


@Component
public class AuditoriumDao {
    private static final String QUERY_PLACES = "select s.id, s.`row`, s.place, r.id booked from seat s " +
            "left join (select * from reservation where reservation.show_id=?) r on s.id = r.seat_id";
    private static final String QUERY_SEAT = "select * from `seat` where `row`=? and `place`=?";

    private DataSource dataSource;

    @Autowired
    public AuditoriumDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public SeatEntity getSeat(int row, int place) {
        RowMapper<SeatEntity> mapper = new BeanPropertyRowMapper<>(SeatEntity.class);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SeatEntity result = jdbcTemplate.queryForObject(QUERY_SEAT, mapper, row, place);
        return result;
    }

    public Collection<SeatEntity> getSeats(long showId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet rows = jdbcTemplate.queryForRowSet(QUERY_PLACES, showId);

        List<SeatEntity> seats = new ArrayList<>();
        while (rows.next()) {
            SeatEntity seat = new SeatEntity();
            seat.setId(rows.getInt(1));
            seat.setRow(rows.getInt(2));
            seat.setPlace(rows.getInt(3));
            seat.setStatus(rows.getObject(4) == null ? SeatStatus.FREE.ordinal() : SeatStatus.RESERVED.ordinal());
            seats.add(seat);
        }
        return seats;
    }
}
