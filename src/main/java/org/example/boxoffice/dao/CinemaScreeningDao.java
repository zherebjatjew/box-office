package org.example.boxoffice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

@Component
public class CinemaScreeningDao {
    private static final String SELECT_ALL_SHOWS = "select id, movie, begins, ends from `show`";
    private static final String SELECT_AVAILABLE_SHOWS = "select id, movie, begins, ends from `show` where `begins` >= ?";

    private DataSource dataSource;

    @Autowired
    public CinemaScreeningDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ShowEntity getShow(long showId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("select id, movie, begins, ends from `show` where id=?",
                resultSet -> resultSet.next() ? mapToShow(resultSet) : null, showId);
    }

    public Collection<ShowEntity> getShows() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Collection<ShowEntity> shows = jdbcTemplate.query(SELECT_ALL_SHOWS, (resultSet, i) -> mapToShow(resultSet));
        return shows;
    }

    public Collection<ShowEntity> getActiveShows(LocalDateTime from) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Collection<ShowEntity> shows = jdbcTemplate.query(SELECT_AVAILABLE_SHOWS, (resultSet, i) -> mapToShow(resultSet), from);
        return shows;
    }

    private ShowEntity mapToShow(ResultSet resultSet) throws SQLException {
        ShowEntity entity = new ShowEntity();
        entity.setId(resultSet.getLong(1));
        entity.setMovie(resultSet.getString(2));
        entity.setStarts(new Timestamp(resultSet.getDate(3).getTime()).toLocalDateTime());
        entity.setEnds(new Timestamp(resultSet.getDate(4).getTime()).toLocalDateTime());
        return entity;
    }
}
