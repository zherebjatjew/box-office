package org.example.boxoffice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.HashMap;
import java.util.Map;


@Component
public class ReservationDao {
    private static final String QUERY_RESERVATION
            = "select r.`id`, `seat_id`, r.`show_id`, r.`email`, r.`natural_key`, s.`place`, s.`row` from " +
            "`reservation` r inner join `seat` s on r.`seat_id` = s.`id` where r.`natural_key`=?";
    public static final String INSERT_RESERVATION = "insert into `reservation`(`seat_id`, `email`, `paid`, `show_id`, `natural_key`) values (?, ?, false, ?, ?)";

    private DataSource dataSource;

    @Autowired
    public ReservationDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<ReservationEntity> getReservations(String reservationKey) {
        RowMapper<ReservationEntity> mapper = new BeanPropertyRowMapper<>(ReservationEntity.class);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List<ReservationEntity> result = jdbcTemplate.query(QUERY_RESERVATION, mapper, reservationKey);
        return result;
    }

    public long createReservation(long showId, long seatId, String email, String naturalKey) {
        SimpleJdbcInsert insert = new SimpleJdbcInsert(dataSource);
        Map<String, Object> parameters = new HashMap<>(1);
        parameters.put("seat_id", seatId);
        parameters.put("email", email);
        parameters.put("paid", false);
        parameters.put("show_id", showId);
        parameters.put("natural_key", naturalKey);

        Number id = insert.withTableName("reservation").usingGeneratedKeyColumns("id").executeAndReturnKey(parameters);
        return (long) id;
    }

    public void createReservations(String email, String key, List<ReservationItemEntity> items) throws SQLException {
        try (PreparedStatement statement = dataSource.getConnection().prepareStatement(INSERT_RESERVATION)) {
            for (ReservationItemEntity item : items) {
                statement.setLong(1, item.getSeatId());
                statement.setString(2, email);
                statement.setLong(3, item.getShowId());
                statement.setString(4, key);
                statement.addBatch();
            }
            statement.executeBatch();
        }
    }
}
