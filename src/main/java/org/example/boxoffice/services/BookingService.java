package org.example.boxoffice.services;

import org.example.boxoffice.api.*;
import org.example.boxoffice.dao.*;
import org.h2.jdbc.JdbcBatchUpdateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Transactional
public class BookingService {
    private static final Logger LOG = LoggerFactory.getLogger(BookingService.class);

    private AuditoriumDao auditoriumDao;
    private CinemaScreeningDao screeningDao;
    private ReservationDao reservationDao;

    // Booking for shows that begin within next {freezeHours} will be blocked
    @Value("${reservation.frozen-period:1}")
    private int freezeHours;

    @Autowired
    public BookingService(AuditoriumDao auditoriumDao, CinemaScreeningDao screeningDao, ReservationDao reservationDao) {
        this.auditoriumDao = auditoriumDao;
        this.screeningDao = screeningDao;
        this.reservationDao = reservationDao;
    }

    public Collection<Seat> getSeats(long showId) {
        ShowEntity show = screeningDao.getShow(showId);
        if (show == null) {
            throw new EntityNotFoundException("Show was not found");
        }
        boolean expired = !dateIsValidForBooking(show.getStarts());
        Collection<SeatEntity> seats = auditoriumDao.getSeats(showId);
        return seats.stream()
                .map((SeatEntity item) -> convertToSeat(item, expired))
                .collect(Collectors.toList());
    }

    public String book(Reservation reservation) {
        if (reservation.getItems() == null || reservation.getItems().isEmpty()) {
            throw new IllegalArgumentException("Reservation list items must be provided");
        }
        List<ReservationItemEntity> itemsToCreate = reservation.getItems().stream().map(item -> {
            ReservationItemEntity entity = new ReservationItemEntity();
            SeatEntity seat = getSeatEntity(item.getRow(), item.getPlace());
            entity.setSeatId(seat.getId());
            ShowEntity show = getShowEntity(item.getShowId());
            if (dateIsValidForBooking(show.getStarts())) {
                entity.setShowId(item.getShowId());
                return entity;
            } else {
                throw new IllegalArgumentException("It is too late to book places for this show");
            }
        }).collect(Collectors.toList());
        String uid = UUID.randomUUID().toString();
        try {
            reservationDao.createReservations(reservation.getEmail(), uid, itemsToCreate);
        } catch (JdbcBatchUpdateException e) {
            throw new DuplicateKeyException("The place is already booked");
        } catch (SQLException ex) {
            throw new RuntimeException("Something went wrong while writing data", ex);
        }
        return uid;
    }

    private ShowEntity getShowEntity(long showId) {
        ShowEntity show = screeningDao.getShow(showId);
        if (show == null) {
            throw new IllegalArgumentException("Show was not found");
        }
        return show;
    }

    private SeatEntity getSeatEntity(int row, int place) {
        try {
            return auditoriumDao.getSeat(row, place);
        } catch (EmptyResultDataAccessException e) {
            String err = "Seat at row " + row + " and " + place + " does not exists";
            throw new IllegalArgumentException(err);
        }
    }

    private boolean dateIsValidForBooking(LocalDateTime showStartDateTime) {
        return showStartDateTime.isAfter(getClosestValidStart());
    }

    private LocalDateTime getClosestValidStart() {
        return LocalDateTime.now().plus(freezeHours, ChronoUnit.HOURS);
    }

    private Seat convertToSeat(SeatEntity item, boolean expired) {
        Seat seat = new Seat();
        seat.setRow(item.getRow());
        seat.setPlace(item.getPlace());
        SeatStatus[] states = SeatStatus.values();
        if (expired) {
            seat.setStatus(SeatStatus.UNAVAILABLE);
        } else {
            int statusId = item.getStatus();
            if (statusId < 0 || statusId >= states.length) {
                LOG.warn("Invalid status of seat {}: {}", item.getId(), statusId);
                seat.setStatus(SeatStatus.UNAVAILABLE);
            } else {
                seat.setStatus(states[statusId]);
            }
        }
        return seat;
    }

    public Collection<Show> getShows(boolean onlyActive) {
        Collection<ShowEntity> entities;
        if (onlyActive) {
            entities = screeningDao.getActiveShows(getClosestValidStart());
        } else {
            entities = screeningDao.getShows();
        }
        return entities.stream().map(this::mapEntityToShow).collect(Collectors.toList());
    }

    public Show getShow(long showId) {
        ShowEntity entity = screeningDao.getShow(showId);
        if (entity == null) {
            throw new EntityNotFoundException("Show was not found");
        }
        return mapEntityToShow(entity);
    }

    public Reservation getReservation(String reservationKey) {
        List<ReservationEntity> entities = reservationDao.getReservations(reservationKey);
        if (entities.isEmpty()) {
            throw new EntityNotFoundException("Reservation does not exist");
        }
        Reservation result = new Reservation();
        result.setEmail(entities.get(0).getEmail());
        result.setItems(entities.stream().map(this::mapEntityToReservationItem).collect(Collectors.toList()));
        return result;
    }

    private Show mapEntityToShow(ShowEntity entity) {
        Show show = new Show();
        show.setId(entity.getId());
        show.setMovie(entity.getMovie());
        show.setStarts(entity.getStarts());
        show.setEnds(entity.getEnds());
        return show;
    }

    private ReservationItem mapEntityToReservationItem(ReservationEntity listItem) {
        ReservationItem dto = new ReservationItem();
        dto.setPlace(listItem.getPlace());
        dto.setRow(listItem.getRow());
        dto.setShowId(listItem.getShowId());
        return dto;
    }
}
